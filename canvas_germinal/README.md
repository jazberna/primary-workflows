## [Germline Copy Number Variation using CANVAS](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/canvas_germinal):
- **What is this?** This workflow runs [CANVAS SmallPedigree-WGS](https://github.com/Illumina/canvas).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/canvas_germinal/):
   * the actual snakemake workflow file [canvas_germinal.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/canvas_germinal/canvas_germinal.sm)
   * the shell script that the user will eventually use to run this workflow [canvas_germinal.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/canvas_germinal/canvas_germinal.sh)
  
- **How do I run this thing?** The following example creates a screen session named 'jorgez_canvas_germinal' runs the script (canvas_germinal.sh) and dettaches from the screen. canvas_germinal.sh takes in this order:
	- full path to bam file 
	- full path to canvas reference folder (/mnt/netapp2/uscmg_aplic/2_library/canvas)
	- full path to referene genome fasta file (/mnt/netapp2/uscmg_aplic/2_library/canvas/kmer.fa)
	- full path to population-b-allele-vcf (/mnt/netapp2/uscmg_aplic/2_library/canvas/dbsnp.vcf)
	- full path to excluded regions file (/mnt/netapp2/uscmg_aplic/2_library/canvas/filter13.bed)
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.
   I would do run the pipeline canvas_germinal.sh like this:
	
```
	screen -c /mnt/lustre/scratch/home/usc/mg/jzb/CANVAS2/CANVAS/primary_pipelines/canvas_germinal/screenrc \
	-L -S jorgez_canvas_germinal -dm /bin/bash /mnt/lustre/scratch/home/usc/mg/jzb/CANVAS2/CANVAS/primary_pipelines/canvas_germinal/canvas_germinal.sh \
	/mnt/lustre/scratch/home/usc/mg/jzb/PROJECT_NAME/2_alignment/SAMPLE/aligned.sorted.marked_duplicates.bam \
	/mnt/netapp2/uscmg_aplic/2_library/canvas \
	/mnt/netapp2/uscmg_aplic/2_library/canvas/kmer.fa \
	/mnt/netapp2/uscmg_aplic/2_library/canvas/dbsnp.vcf \
	/mnt/netapp2/uscmg_aplic/2_library/canvas/filter13.bed \
	priority  
```
 
- **Where are the logs?**  The logs you are interested in are produced in the working directory for this this workflow which has this pattern:
```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/7_cnv/SAMPLE/logs/canvas_germinal
```
The SLURM logs would be:

```
	canvas_germinal.JOBID.err.TIMESTAMP
	canvas_germinal.JOBID.out.TIMESTAMP
	plot_canvas_germinal.JOBID.err.TIMESTAMP
	plot_canvas_germinal.JOBID.out.TIMESTAMP
```

  There will two extra logs that contain the version (commit) used to run the workflow:

```
    canvas_germinal.log.TIMESTAMP
    plot_canvas_germinal.log.TIMESTAMP
    canvas_germinal.move_logs.log.TIMESTAMP
```

  The canvas_germinal.log.TIMESTAMP files is useful to see in one go what steps were done, it reads something like:
	
```
	#DONE_CANVAS_GERMINAL
	commit_7b876321e698e7af3a96f1ba432e39c56bb6d02a
```
    The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/canvas_germinal-TIMESTAMP-session-name
```
- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'canvas_germinal' step failed because of memory (canvas run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.

- **I forgot the most important bit. Where/What's the output?**

  Let's sat your working ditectory is again:

```
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/7_cnv/SAMPLE
```

  Then you will find the output:

```
    CNV.vcf.gz
    canvas_germinal.pdf
```