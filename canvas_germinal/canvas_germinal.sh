#!/bin/bash
#-----------------------------------------------------------------
# canvas_germinal.sh
# 
#
# Parameters : 
# $1: full path to tumour bam file 
# $2: full path to canvas reference folder
# $3: full path to referene genome fasta file
# $4: population-b-allele-vcf
# $5: full path to excluded regions file
# $6: priority mode (normal,priority,xl,minimum) or custom config json file
# $7: --dryrun
#-----------------------------------------------------------------


# revert the command rm seccurity measure
unalias rm

# exit when any command fails
set -e

THIS_DIR=$(dirname $0)
THIS_PIPELINE=$(basename $THIS_DIR)

# print last commit information
cd $THIS_DIR
VERSION=`git log | head -n1 | sed 's/ /_/g'`


CLUSTER_CONFIG_JSON=`/bin/bash ${THIS_DIR}/../utils/apply_cluster_config.sh ${6} ${THIS_PIPELINE}` 
echo $CLUSTER_CONFIG_JSON
export CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_JSON

module load dotnet-sdk/2.1.700
module load gcc/6.4.0 R/3.6.0

export LD_LIBRARY_PATH=$APLICACIONES/3_environments/python/canvas/lib:$LD_LIBRARY_PATH
export R_LIBS=/mnt/netapp2/uscmg_aplic/4_R_libraries:$R_LIBS
export CANVAS="$APLICACIONES/0_external_tools/canvas/Canvas-1.40.0.1613+master_x64/Canvas.dll"
export CANVAS_VIEWER="$APLICACIONES/1_internal_tools/12_labclinico-bioinfo/canvas_germinal/vcf_viewer.R"


$APLICACIONES/0_external_tools/miniconda/3.6/bin/snakemake --snakefile ${THIS_DIR}/canvas_germinal.sm  \
--config bam=${1} genome_folder=${2} reference=${3} population_b_allele_vcf=${4} filter_bed=${5} version=${VERSION} \
--cluster-config $CLUSTER_CONFIG_JSON \
--cluster "sbatch -p {cluster.p}  -A {cluster.account} --error {cluster.error} --output {cluster.output} --time  {cluster.time} --mem {cluster.mem} -c {cluster.c}" \
--printshellcmds  --rerun-incomplete --verbose --latency-wait 60 --jobs 1 \
${7}