#!/bin/bash
#-----------------------------------------------------------------
# primary.sh
# 
#
# Parameters : 
# $1: full path to referene genome fasta file
# $2: full path to fastq1 file
# $3: full path to fastq2 file 
# $4: priority mode (normal,priority,xl,minimum) or custom config json file
# $5: posfix sample name 
# $6: --dryrun
#-----------------------------------------------------------------

# revert the command rm seccurity measure
unalias rm

# exit when any command fails
#set -e

THIS_DIR=$(dirname $0)
THIS_PIPELINE=$(basename $THIS_DIR)

# print last commit information
cd $THIS_DIR
VERSION=`git log | head -n1 | sed 's/ /_/g'`


export PATH=/mnt/netapp2/uscmg_aplic/0_external_tools/biobambam2/biobambam2/2.0.87-release-20180301132713/x86_64-etch-linux-gnu/bin:$PATH

ID=`/bin/bash ${THIS_DIR}/../utils/get_id_from_fastq.sh ${2}` 
CLUSTER_CONFIG_JSON=`/bin/bash ${THIS_DIR}/../utils/apply_cluster_config.sh ${4} ${THIS_PIPELINE}` 

echo $ID
echo $CLUSTER_CONFIG_JSON

export CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_JSON

/mnt/netapp2/uscmg_aplic/0_external_tools/miniconda/3.6/bin/snakemake --snakefile ${THIS_DIR}/primary.sm \
--config reference=${1} fastq1=${2} fastq2=${3} id=${ID} version=${VERSION} posfix=${5} \
--cluster-config $CLUSTER_CONFIG_JSON \
--cluster "sbatch -p {cluster.p}  -A {cluster.account} --error {cluster.error} --output {cluster.output} --time  {cluster.time} --mem {cluster.mem} -c {cluster.c}" \
--printshellcmds --jobs --verbose --rerun-incomplete --latency-wait 60 --nolock \
${6}
