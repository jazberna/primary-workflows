## [primary analysis pipeline:](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/primary)

- **What is this?** This pipeline aligns illumina paired fastq files then sorts, marks duplicates and indexes the resulting bam file.  
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/primary):
   
   * the actual snakemake workflow file [primary.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/primary/primary.sm)

   * the shell script that the user will eventually use to run this workflow: [primary.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/primary/primary.sh)

- **How do I run this thing?**
  The following example creates a screen session named 'jorgez' runs the pipeline (primary.sh) and dettaches from the screen. primary.sh takes in this order:
  - the reference genome.
  - fastq1 file. 
  - fastq2 file.
  - cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
  - posfix sample name. This is appended to the sample name i.e. to indicate something like mapped to a different reference. IMPORTANT IF YOU DO NOT WANT POSFIX YOU MUST SET: ''
  - --dryrun : if does a dry run  
  I would do run the pipeline [primary.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/primary/primary.sh) like this:

	```
	screen -c /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/primary/screenrc \
	-L -S jorgez -dm /bin/bash /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/primary/primary.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/1_sequencing/HS118/HS118_R1.fastq.gz \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/1_sequencing/HS118/HS118_R2.fastq.gz \
	priority \
	alternative_alignment
	```

- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:

    ```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/2_alignment/SAMPLE_NAME/logs
    ```
    
    for example:
        
    ```
    /mnt/netapp2/mobilegenomes/0/1_projects/1020_CELL-LINES-PE/2_alignment/BL2087/logs
    ```
    The logs will appear in such working directory. There will be five SLURM logs (.out.timestamp and .err.timestamp) per task namely:
  
    ```
    align.jobid.out.timestamp
  
    align.jobid.err.timestamp
  
    sort.jobid.out.timestamp
  
    sort.jobid.err.timestamp
  
    mark_duplicates.jobid.out.timestamp
  
    mark_duplicates.jobid.err.timestamp
  
    cleanup.jobid.out.timestamp
  
    cleanup.jobid.err.timestamp
  
    move_logs.jobid.out.timestamp
  
    move_logs.jobid.err.timestamp
    ```
  
    If each of those tasks completes successfully there will be another set of .log files inside that directory. The *log files are useful to see in one go what steps where done. For instance the cleanup.log file only contains the string 'DONE_CLEANUP'.

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/primary-TIMESTAMP-session-name
```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'align' step failed because of memory (bwa mem run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed. 

- **I forgot the most important bit. Where/What's the output?**

	Let's sat your working ditectory is again:
    ```
    /mnt/netapp2/mobilegenomes/0/1_projects/1020_CELL-LINES-PE/2_alignment/CLD064T
    ```
 
  Then you will find there, alongside with the logs, the bam file with duplicates marked plus its index file.
    
    ```  
    aligned.sorted.marked_duplicates.bam
  
    aligned.sorted.marked_duplicates.bam.bai
    ```

