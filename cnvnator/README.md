## [Germline Copy Number Variation using CNVnator](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/cnvnator):
- **What is this?** This workflow runs [CNVnator](https://github.com/abyzovlab/CNVnator).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/cnvnator):
   * the actual snakemake workflow file [cnvnator.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/cnvnator/cnvnator.sm)
   * the shell script that the user will eventually use to run this workflow [cnvnator.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/cnvnator/cnvnator.sh)

- **How do I run this thing?** The following example creates a screen session named 'jorgez_cnvnator' runs the script (cnvnator.sh) and dettaches from the screen. cnvnator.sh takes in this order:
	- full path to bam file 
	- full path to referene genome fasta file
	- bin size (recommended 100)
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.

    I would do run the pipeline canvas_germinal.sh like this:
	
```
	screen -c $PATH_TO/primary_pipelines/cnvnator/screenrc \
	-L -S jorgez_cnvnator -dm /bin/bash $PATH_TO/primary_pipelines/cnvnator/cnvnator.sh \
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/2_alignment/SAMPLE/aligned.sorted.marked_duplicates.bam \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	100 \
	priority
```
 
- **Where are the logs?**  The logs you are interested in are produced in the working directory for this this workflow which has this pattern:
```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/7_cnv/SAMPLE/logs/cnvnator
```
The SLURM logs would be:

```
cnvnator_read_extraction.JOBID.out.TIMESTAMP
cnvnator_read_extraction.JOBID.err.TIMESTAMP
cnvnator_post_read_extraction.JOBID.err.TIMESTAMP
cnvnator_post_read_extraction.JOBID.out.TIMESTAMP
cnvnator_index_vcf.JOBID.err
cnvnator_index_vcf.JOBID.out
```

  There will two extra logs that contain the version (commit) used to run the workflow:

```
cnvnator_post_read_extraction.log.TIMESTAMP
cnvnator_index_vcf.log.TIMESTAMP
cnvnator_move_logs.log.TIMESTAMP
cnvnator_post_read_extraction.log.TIMESTAMP
cnvnator_read_extraction_CHR.log.TIMESTAMP
```

  The canvas_germinal.log.TIMESTAMP files is useful to see in one go what steps were done, it reads something like:
	
```
	#DONE_CNVNATOR
	commit_7b876321e698e7af3a96f1ba432e39c56bb6d02a
```

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/cnvnator-TIMESTAMP-session-name
```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'canvas_germinal' step failed because of memory (canvas run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.

- **I forgot the most important bit. Where/What's the output?**

  Let's sat your working ditectory is again:

```
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/7_cnv/SAMPLE
```

  Then you will find the output:

```
cnvnator.root
cnvnator.calls.tsv
cnvnator.eval.txt
cnvnator.vcf.gz
cnvnator.vcf.gz.tbi
```
