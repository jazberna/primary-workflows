#!/bin/bash
#-----------------------------------------------------------------
# cnvnator.sh
# 
#
# Parameters : 
# $1: full path to tumour bam file 
# $2: full path to referene genome fasta file
# $3: bin size (recommended 100)
# $4: priority mode (normal,priority,xl,minimum) or custom config json file
# $5: --dryrun
#-----------------------------------------------------------------


# revert the command rm seccurity measure
unalias rm

# exit when any command fails
set -e

THIS_DIR=$(dirname $0)

THIS_PIPELINE=$(basename $THIS_DIR)

# print last commit information
cd $THIS_DIR
VERSION=`git log | head -n1 | sed 's/ /_/g'`

CLUSTER_CONFIG_JSON=`/bin/bash ${THIS_DIR}/../utils/apply_cluster_config.sh ${4} ${THIS_PIPELINE}` 

echo $CLUSTER_CONFIG_JSON
export CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_JSON

module load perl
module load gcccore/6.4.0 root/6.10.08-python-2.7.15

export PATH=$APLICACIONES/0_external_tools/tabix-0.2.6:$PATH
export PATH=$APLICACIONES/0_external_tools/cnvnator/CNVnator:$PATH

$APLICACIONES/0_external_tools/miniconda/3.6/bin/snakemake --snakefile ${THIS_DIR}/cnvnator.sm  \
--config bam=${1} reference=${2} bin_size=${3} version=${VERSION} \
--cluster-config $CLUSTER_CONFIG_JSON \
--cluster "sbatch -p {cluster.p}  -A {cluster.account} --error {cluster.error} --output {cluster.output} --time  {cluster.time} --mem {cluster.mem} -c {cluster.c}" \
--printshellcmds  --rerun-incomplete --verbose --latency-wait 60  --nolock --jobs 1 \
${5}
