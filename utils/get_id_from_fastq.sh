#!/bin/bash

# get ID for @RG 
FIELDS=`zcat ${1} | head -n1 | awk '{print $1}' | grep -o ":" | wc -l` 
if [ $FIELDS == 6 ]
then
    # get instrument and lane
    ID=`zcat ${1} | head -n1 | cut -d':' -f3,4 | sed  's/:/./'`
elif [ $FIELDS == 4 ]
then
    # get flowcel and lane
    ID=`zcat ${1} | head -n1 | cut -d':' -f1,2 | sed 's/:/./' | sed 's/@//g'`

elif [ $FIELDS == 0 ] # must be ENA
then
    # get SSR ads the ID
    ID=`zcat ${1} | head -n1 | awk -F'[@.]' '{print $2}'`
else
    echo "could not find flowcell and lane from fatsq ${1}"
    exit 1
fi

echo $ID

