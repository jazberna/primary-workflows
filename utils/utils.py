import os
import pprint
import json

cluster_json_file=os.environ['CLUSTER_CONFIG_JSON']

# get cpus for multithread applications and print cluster config
with open(cluster_json_file) as handle:
    dictdump = json.loads(handle.read())
    print('******** CLUSTER_CONFIG ******************')
    pprint.pprint(dictdump)
    print('******************************************')


print('******** USER_CONFIG ******************')
pprint.pprint(config)
print('***************************************')
