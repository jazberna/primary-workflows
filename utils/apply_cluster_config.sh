#!/bin/bash

CLUSTER_MODE=$1
PIPELINE=$2

THIS_DIR=$(dirname $0)
CLUSTER_CONFIG_DIR=${THIS_DIR}/../${PIPELINE}/cluster_configuration

if [ -f $CLUSTER_MODE ] 
then 
    #using user defined cluster config file
    CLUSTER_CONFIG_JSON=$CLUSTER_MODE

elif [ $CLUSTER_MODE == "minimum" ];
then
    CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_DIR/cluster_minimum.json
  
elif [ $CLUSTER_MODE = "normal" ];
then
    CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_DIR/cluster.json

elif [ $CLUSTER_MODE = "xl" ];
then
    CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_DIR/cluster_xl.json

elif [ $CLUSTER_MODE = "priority" ];
then
    CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_DIR/cluster_priority.json
fi


if  [ ! -f $CLUSTER_CONFIG_JSON ];
then 
    echo "cluster file does not exists"
    echo $CLUSTER_CONFIG_JSON
    echo 'exiting'
    exit 1
else	
    echo ${CLUSTER_CONFIG_JSON}
fi
