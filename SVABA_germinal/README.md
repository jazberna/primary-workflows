## [Structural Variation using SvABA](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SVABA_germinal):
- **What is this?** This workflow runs [SvABA SV and indel caller](https://github.com/walaj/svaba).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SVABA_germinal):
   * the actual snakemake workflow file [svaba_germinal.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SVABA_germinal/svaba_germinal.sm)
   * the shell script that the user will eventually use to run this workflow [svaba_germinal.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SVABA_germinal/svaba_germinal.sh)
  
- **How do I run this thing?** The following example creates a screen session named 'jorgez_svaba_germinal' runs the script (svaba_germinal.sh) and dettaches from the screen. svaba_germinal.sh takes in this order:
	- full path to referene genome fasta file
	- full path to bam file 
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.
   
    I would do run the pipeline svaba_germinal.sh like this:
	
```
	screen -c PATH_TO/SVABA_germinal/screenrc \
	-L -S jorgez_svaba_germinal -dm /bin/bash PATH_TO/SVABA_germinal/svaba_germinal.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/2_alignment/HS118/aligned.sorted.marked_duplicates.bam \
	priority
```
 
- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:
```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME/logs/svaba_germinal
```
The SLURM logs would be:

```  
    svaba_germinal.JOBID.out.TIMESTAMP
    svaba_germinal.JOBID.err.TIMESTAMP
    svaba_germinal.index_vcf.JOBID.out.TIMESTAMP
    svaba_germinal.index_vcf.JOBID.err.TIMESTAMP
    svaba_germinal.move_logs.JOBID.out.TIMESTAMP
    svaba_germinal.move_logs.JOBID.err.TIMESTAMP
```  
  
  There will be three extra logs that contain the version (commit) used to run the workflow:
 
```
    svaba_germinal.index_vcf.log.TIMESTAMP
    svaba_germinal.log.TIMESTAMP
    svaba_germinal.move_logs.log.TIMESTAMP
```
  
  The *log.TIMESTAMP files are useful to see in one go what steps where done. For instance the svaba_germinal.log.TIMESTAMP reads something like:
	
```
    #DONE_SVABA_GERMINAL
    commit_f63a36e4d44399117dc62f891dbba6b623cc4cfb
```
    The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/SVABA_germinal-TIMESTAMP-session-name
```


- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'svaba_germinal' step failed because of memory (svaba run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.
 

- **I forgot the most important bit. Where/What's the output?**

  Let's sat your working ditectory is again:

```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME
```
   
  Then you will find the output:

```
    SAMPLE_NAME.svaba.sv.vcf.gz
    SAMPLE_NAME.svaba.indel.vcf.gz
    SAMPLE_NAME.svaba.sv.vcf.gz.tbi
    SAMPLE_NAME.svaba.indel.vcf.gz.tbi
```