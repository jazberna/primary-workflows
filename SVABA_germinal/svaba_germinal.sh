#!/bin/bash
#-----------------------------------------------------------------
# svaba_germinal.sh
# 
#
# Parameters : 
# $1: full path to referene genome fasta file
# $2: full path to bam file
# $3: priority mode (normal,priority,xl,minimum) or custom config json file
# $4: --dryrun
#-----------------------------------------------------------------

# revert the command rm seccurity measure
unalias rm

# exit when any command fails
set -e

THIS_DIR=$(dirname $0)
THIS_PIPELINE=$(basename $THIS_DIR)


# print last commit information
cd $THIS_DIR
VERSION=`git log | head -n1 | sed 's/ /_/g'`

CLUSTER_CONFIG_JSON=`/bin/bash ${THIS_DIR}/../utils/apply_cluster_config.sh ${3} ${THIS_PIPELINE}` 
echo $CLUSTER_CONFIG_JSON
export CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_JSON

export PATH=/mnt/netapp2/uscmg_aplic/0_external_tools/tabix-0.2.6:$PATH

export PATH=/mnt/netapp2/uscmg_aplic/0_external_tools/svaba/bin:$PATH
export LD_LIBRARY_PATH=/mnt/netapp2/uscmg_aplic/0_external_tools/miniconda/3.6/lib:$LD_LIBRARY_PATH
module load bzip2/1.0.6

/mnt/netapp2/uscmg_aplic/0_external_tools/miniconda/3.6/bin/snakemake --snakefile ${THIS_DIR}/svaba_germinal.sm  \
--config genome=${1} bam=${2} version=${VERSION} \
--cluster-config $CLUSTER_CONFIG_JSON \
--cluster "sbatch -p {cluster.p}  -A {cluster.account} --error {cluster.error} --output {cluster.output} --time  {cluster.time} --mem {cluster.mem} -c {cluster.c}" \
--printshellcmds --verbose --rerun-incomplete --latency-wait 60 --nolock \
--jobs 1 \
$4

