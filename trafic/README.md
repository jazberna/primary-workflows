## [TraFiC ](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/trafic):
- **What is this?** This workflow allows to run [TraFiC](https://gitlab.com/mobilegenomesgroup/TraFiC) at CESGA directly without docker.
  
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/trafic)
   
   * the actual snakemake workflow file [SnakefileTraficHg19CESGA.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/trafic/SnakefileTraficHg19CESGA.sm)

   * the shell script that the user will eventually use to run this workflow: [trafic.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/trafic/trafic.sh)

   * **How do I run this thing?** The following example creates a screen session named 'jorgez_trafic' that runs the pipeline and dettaches from the screen. trafic.sh takes **in this order**.
      - genome: the full path to the reference genome
      - normal_bam: full name for the normal bam file
      - tumour_bam: full name for the tumour bam file
      - priorty mode (normal or priority)
      - --dryrun: if you want to do a dry run first 

```
    screen -c /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/trafic/screenrc \
    -L -S jorgez_trafic -dm /bin/bash /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/trafic/trafic.sh \
    /mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
    /mnt/lustre/scratch/home/usc/mg/jzb/TESTMUTECT2SPARK/4000_TRAFIC/2_alignment/HCC1143_T/aligned.sorted.marked_duplicates.bam \
    /mnt/lustre/scratch/home/usc/mg/jzb/TESTMUTECT2SPARK/4000_TRAFIC/2_alignment/HCC1143_N/aligned.sorted.marked_duplicates.bam \
    priority
```

- **Where are the logs?** Apart from whichever logs TraFiC/TEIBA produce the logs you are also interested in are produced in the working directory for this this workflow which has this pattern:

```
    /mnt/netapp2/mobilegenomes/0/1_projects/PROJECT_NAME/3_trafic/TUMOUR_SAMPLE/3_trafic/TUMOUR_SAMPLE_NORMAL_SAMPLE/logs/trafic
```  
  for example:
  
```
    /mnt/netapp2/mobilegenomes/0/1_projects/1020_CELL-LINES-PE/3_trafic/HCC1143_T_HCC1143_N/logs/trafic  
```
  
  The logs will appear in such working directory. There will be two SLURM logs (.out.timestamp and .err.timestamp) per task. There will be also *log.timestamp files useful to see in one go what steps where done. For instance the normal_bwamem.log.timestamp file only contains the string '#DONE_normal_bwamem' and the version (commit) of the pipeline.
  
```
    trafic.move_logs.log
    tumour_samtools.log.TIMESTAMP
    normal_samtools.log.TIMESTAMP
    normal_bwamem.log.TIMESTAMP
    tumour_bwamem.log.TIMESTAMP
    tumour_split_by_element_type.log.TIMESTAMP
    normal_split_by_element_type.log.TIMESTAMP
    tumour_clustering.log.TIMESTAMP
    normal_clustering.log.TIMESTAMP
    tumour_cluster_MGE.log.TIMESTAMP
    normal_TEs.log.TIMESTAMP
    tumour_transductions.log.TIMESTAMP
    normal_transductions.log.TIMESTAMP
    tumour_cluster_transductions.log.TIMESTAMP
    normal_cluster_transductions.log.TIMESTAMP
    t0_t1_td1_t2_td2.log.TIMESTAMP
    get_teiba_input.log.TIMESTAMP
    run_teiba_pipeline.log.TIMESTAMP
```

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/trafic-TIMESTAMP-SESSION_NAME
```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'normal_bwamem' step failed because of memory (bwa mem run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.

- **I forgot the most important bit. Where/What's the output?**

    The output will be in:
    
```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/3_trafic/TUMOUR_SAMPLE/TUMOUR_SAMPLE_NORMAL_SAMPLE
```
  
	For example:
    
```
    /mnt/netapp2/mobilegenomes/0/1_projects/1020_CELL-LINES-PE/3_trafic/HCC1143_T/HCC1143_T_HCC1143_N
```
