## [Structural Variation using lumpy+smoove](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/smoove_germinal):
- **What is this?** This workflow runs [lumpy SV caller with smoove wrapper](https://github.com/brentp/smoove).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/smoove_germinal):
   * the actual snakemake workflow file [smoove_germinal.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/smoove_germinal/smoove_germinal.sm)
   * the shell script that the user will eventually use to run this workflow [smoove_germinal.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/smoove_germinal/smoove_germinal.sh)

- **How do I run this thing?** The following example creates a screen session named 'jorgez_smoove_germinal' runs the script (smoove_germinal.sh) and dettaches from the screen. smoove_germinal.sh takes in this order:
	- full path to referene genome fasta file
	- full path to bam file 
	- full path to exluded regions file 
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.
   I would do run the pipeline smoove_germinal.sh like this:
	
```
	screen -c PATH_TO/primary_pipelines/smoove_germinal/screenrc \
	-L -S jorgez_smoove_germinal -dm /bin/bash PATH_TO/primary_pipelines/smoove_germinal/smoove_germinal.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	/mnt/lustre/scratch/home/usc/mg/jzb/TESTSMOOVE/PROJECT/2_alignment/SAMPLE/aligned.sorted.marked_duplicates.bam \
	/mnt/netapp2/uscmg_aplic/2_library/smoove/ceph18.b37.lumpy.exclude.2014-01-15.bed \
	priority
```

- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:
```
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME/logs/smoove_germinal
```
The SLURM logs would be:

```  
	smoove_germinal.index_vcf.JOBID.err.TIMESTAMP
	smoove_germinal.index_vcf.JOBID.out.TIMESTAMP
	smoove_germinal.move_logs.JOBID.err.TIMESTAMP
	smoove_germinal.move_logs.JOBID.out.TIMESTAMP
	smoove_germinal.JOBID.err.TIMESTAMP
	smoove_germinal.JOBID.out.TIMESTAMP
```  

  There will be three extra logs that contain the version (commit) used to run the workflow:

```
	smoove_germinal.index.log.TIMESTAMP
	smoove_germinal.log.TIMESTAMP
	smoove_germinal.move_logs.log.TIMESTAMP
```

  The *log.TIMESTAMP files are useful to see in one go what steps where done. For instance the smoove_germinal.log.TIMESTAMP reads something like:
	
```
	#DONE_SMOOVE_GERMINAL
	commit_15fa5fba045fab02e63a2d4ae0032bd1ffd8133f
```

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/smoove_germinal-TIMESTAMP-session-name
```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'smoove_germinal' step failed because of memory (smoove run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.
 

- **I forgot the most important bit. Where/What's the output?**

  Let's sat your working ditectory is again:

```
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME
```
   
  Then you will find the output:

```
	smoove_germinal-lumpy-cmd.sh
	smoove_germinal-smoove.genotyped.vcf.gz
	smoove_germinal-smoove.genotyped.vcf.gz.tbi
```