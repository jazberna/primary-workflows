## [SNPs and indels](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SNP_indels):
- **What is this?** This workflow runs runs the [HaplotypeCaller](https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_haplotypecaller_HaplotypeCaller.php).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SNP_indels):
   * the actual snakemake workflow file [SNP_indels.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SNP_indels/SNP_indels.sm)
   * the shell script that the user will eventually use to run this workflow: [SNP_indels.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SNP_indels/SNP_indels.sh)

- **How do I run this thing?**
   The following example creates a screen session named 'jorgez' runs the pipeline (SNP_indels.sh) and dettaches from the screen. SNP_indels.sh takes in this order:

    - the reference genome
    - file with excluded intervals (o an empty file to not exclude anything). One interval per line i.e. 1:100-2399. File extension must be 'intervals'
    - the bam file
    - the number of CPUs (max 24). I recommend 12.
    - cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
    - --dryrun option. add that that if you only want to see the execution plan.
```
	screen -c /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/SNP_indels/screenrc \
	-L -S jorgez -dm /bin/bash /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/SNP_indels/SNP_indels.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	/mnt/netapp2/uscmg_aplic/2_library/haplotypecaller/excluded.intervals \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/2_alignment/HS118/aligned.sorted.marked_duplicates.bam \
	12 \
	priority
```
    
    
    To see how the worfflow is pregressing, just re-attach to the session:

```
	screen -d -r jorge_haplotypecaller
```

    Remember, if you don't remember screen session name you want to re-attach just list them with:

```
    screen -ls
```

 
- **Where are the logs?** The logs will appear in:
    
    ```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/6_snv/SAMPLE_NAME/logs
    ```
    
    for example:
        
    ```
    /mnt/netapp2/mobilegenomes/0/1_projects/1020_CELL-LINES-PE/6_snv/BL2087/logs/haplotypecaller
    ```
    
    The logs will appear in such working directory. There will be five SLURM logs (.out.timestamp and .err.timestamp) per task namely:
    
    ```
    chr.haplotypecaller.jobid.out.timestamp
    ```
    
    If each of those tasks completes successfully there will be another file called unified_genotyper.log inside the working directory, which contains the string 'DONE_UNIFIED_GENOTYPER'.
    
    The screen log will be located in:
    ```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/SNP_indels-TIMESTAMP-session-name
    ```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'align' step failed because of memory (bwa mem run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed. 


- **I forgot the most important bit. Where/What's the output?**

  
    The output will be in:
    
    ```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/6_snv/SAMPLE_NAME
    ```
    
    For example:
    
    ```
    /mnt/netapp2/mobilegenomes/0/1_projects/1020_CELL-LINES-PE/6_snv/CLD064T
    ```


    Then you will find there, the HaplotypeCaller output:
    
    
    ```  
    haplotypecaller.all_chrs.filtered.vcf.idx
  
    haplotypecaller.all_chrs.filtered.vcf.gz
    
    haplotypecaller.all_chrs.filtered.vcf.gz.tbi
    ```
