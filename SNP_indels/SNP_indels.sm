import os
import pprint

position = sys.argv.index('--snakefile')
position+=1
snakemake_file = sys.argv[position]

this_file_path = os.path.dirname(os.path.realpath(snakemake_file))
utils_file = this_file_path+'/../utils/utils.py'

if not os.path.isfile(utils_file):
    sys.stderr.write("Exiting. Could not find {}".format(utils_file))
    exit(1)
else:
    exec(open(utils_file).read())

reference=config['genome']
bam=config['bam']
commit=config["version"]
excluded_intervals=config["excluded_intervals"]

sample_name= bam.split('/')[-2]

output_dir_haplotypecaller = os.path.dirname(bam)+'/../../6_snv/' + sample_name
output_dir_haplotypecaller_logs = output_dir_haplotypecaller+'/logs/haplotypecaller'

CHRS=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 'X', 'Y','MT']


HAPLOTYPECALLER = expand(output_dir_haplotypecaller+'/{chr}.haplotypecaller.vcf.idx', chr=CHRS)

# make output dir and logs subdir
snakemake.utils.makedirs(output_dir_haplotypecaller)
snakemake.utils.makedirs(output_dir_haplotypecaller_logs)

# make logs subdir the workdir
workdir: output_dir_haplotypecaller_logs

import shutil
onsuccess:
    shutil.rmtree(".snakemake",ignore_errors=True)



move_logs = os.path.join(output_dir_haplotypecaller_logs,'haplotypecaller.move_logs.log')

index_log = os.path.join( output_dir_haplotypecaller_logs,'haplotypecaller.index.log')

filtered_vcf =  os.path.join( output_dir_haplotypecaller,'haplotypecaller.all_chrs.filtered.vcf')

filtered_vcf_index = filtered_vcf +'.gz.tbi';

filter_log = os.path.join( output_dir_haplotypecaller_logs,'haplotypecaller.filter.log')

merge_vcf = os.path.join( output_dir_haplotypecaller,'haplotypecaller.all_chrs.vcf')

merge_log = os.path.join( output_dir_haplotypecaller_logs,'haplotypecaller.merge.log')

haplotypecaller_log = os.path.join( output_dir_haplotypecaller_logs,'haplotypecaller.log')

haplotypecaller_cpus = int(dictdump['haplotypecaller']["c"])

vcf_files_to_gather = map( lambda c : '--INPUT=' + output_dir_haplotypecaller +'/' + str(c) + '.haplotypecaller.vcf' , CHRS )
vcf_files_to_gather_str = ' '.join(vcf_files_to_gather)




rule move_logs:
    input:
        filtered_vcf_index
    threads:
        1
    output:
        move_logs
    shell:
        "set -euxo pipefail;"
        "find -name 'haplotypecaller*out' | xargs -I file mv file file.$(date +%Y%m%d%H%M%S);"
        "find -name 'haplotypecaller*err' | xargs -I file mv file file.$(date +%Y%m%d%H%M%S);"
        "find {output_dir_haplotypecaller}/* -name '*vcf' -delete;"
        "find {output_dir_haplotypecaller}/* -name '*idx' -and -not -name '*filtered*' -delete;"
        "find -name '*haplotypecaller*log' | xargs -I file mv file {output_dir_haplotypecaller_logs}/file.$(date +%Y%m%d%H%M%S);"
        "cd {output_dir_haplotypecaller};"
        "find -name '*haplotypecaller*log' | xargs -I file mv file {output_dir_haplotypecaller_logs}/file.$(date +%Y%m%d%H%M%S);"
        "echo '#DONE_MOVE_LOGS' > {move_logs};"
        "echo {commit} >> {move_logs};"
        "sleep 10;"

rule index_vcf:
    input:
        filtered_vcf
    output:
        filtered_vcf_index
    log:
        index_log
    threads:
        1
    shell:
        "set -euxo pipefail;"
        "bgzip -c {filtered_vcf} > {filtered_vcf}.gz;"
        "tabix -p vcf {filtered_vcf}.gz;"
        "echo '#DONE_TABIX' > {log};"
        "echo {commit} >> {log};"
        "sleep 10;"


rule filtrate_haplotypecaller_vcf:
    input:
        merge_vcf
    output:
        filtered_vcf
    log:
        filter_log
    threads:
        1
    shell:
        "set -euxo pipefail;"
        "module load gatk;"
        "gatk VariantFiltration -R {reference} --output {filtered_vcf} --variant {merge_vcf} --exclude-intervals {excluded_intervals};"
        "echo '#DONE_FILTER_VCF' > {log};"
        "echo {commit} >> {log};"
        "sleep 10;"

rule merge_vcfs:
    input:
        HAPLOTYPECALLER
    
    output:
        merge_vcf
    
    log:
        merge_log
    
    threads: 1

    shell:
        "set -euxo pipefail;"
        "module load gatk;"
        "gatk GatherVcfs {vcf_files_to_gather_str} --OUTPUT={merge_vcf};"
        "echo '#DONE_GATHER_VCF' > {log};"
        "echo {commit} >> {log};"
        "sleep 10;"

rule haplotypecaller: 
    input:
        reference,
        bam

    output:
        file='{chr}.haplotypecaller.vcf.idx'

    log:
        file='{chr}.haplotypecaller.log'

    threads: 1

    shell:
        "set -euxo pipefail;"
        "module load gatk;"
        "chr=`basename {output.file} | cut -f1 -d'.'`;"
        "outputvcf=`echo {output.file} | sed s/.idx//`;"
        "gatk HaplotypeCaller -L $chr -R {reference} --input {bam} --output $outputvcf;"
        "echo '#DONE_HAPLOTYPECALLER' $chr > {log.file};"
        "echo {commit} >> {log};"
        "sleep 10;"

localrules: move_logs
