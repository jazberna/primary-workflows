# This project holds a set of [snakemake](https://snakemake.readthedocs.io/en/stable/index.html) workflows:

## [Primary analysis pipeline:](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/primary)

## [SNPs and indels](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/SNP_indels)

## [Germinal SV Delly](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/SV_PR_SR_RD)

## [Germinal CNV lumpy+smoove](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/smoove_germinal)
  
## [Somatic SV Delly](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/somatic_SV)
  
## [TraFiC](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/trafic)
 
## [Somatic SNPs and indels](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/somatic_SNP_indels)

## [Somatic CNV ASCAT](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/ascat)

## [Germinal SV SvAVA](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/SVABA_germinal)

## [Germinal CNV CANVAS](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/canvas_germinal)

## [Germinal CNV CNVnator](https://gitlab.com/mobilegenomesgroup/primary_pipelines/tree/labclinico_bioinfo/cnvnator)
