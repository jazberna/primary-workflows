## [Somatic Structural Variation using Delly](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/somatic_SV)
- **What is this?** This workflow runs [Delly SV caller](https://github.com/dellytools/delly).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/somatic_SV):
   * the actual snakemake workflow file [delly_somatic_SV.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/somatic_SV/delly_somatic_SV.sm)
   * the shell script that the user will eventually use to run this workflow [delly_somatic_SV.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/somatic_SV/delly_somatic_SV.sh)
  
- **How do I run this thing?** The following example creates a screen session named 'jorgez_delly_somatic' runs the pipeline (delly_somatic.sh) and dettaches from the screen. delly_somatic_SV.sh takes in this order:
	- reference genome file
	- excluded regions file
	- tumour bam file 
	- normal bam file 
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.

	
   I would do run the pipeline delly_somatic.sh like this:
	
```
	screen -c /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/somatic_SV/screenrc \
	-L -S jorgez_delly_somatic -dm /bin/bash /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/somatic_SV/delly_somatic_SV.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	/mnt/netapp2/uscmg_aplic/2_library/delly/hg19.excl \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/2_alignment/HS119/aligned.sorted.marked_duplicates.bam \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/2_alignment/HS118/aligned.sorted.marked_duplicates.bam \
	priority
```
 
- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:

```
    /mnt/netapp2/mobilegenomes/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME/logs/delly_somatic
```

This workflow adds these SLURM files:

```  
    delly_somatic.JOBID.out.TIMESTAMP
    delly_somatic.JOBID.err.TIMESTAMP
    delly_somatic.bcf_to_vcf.JOBID.out.TIMESTAMP
    delly_somatic.bcf_to_vcf.JOBID.err.TIMESTAMP
    delly_somatic.index_vcf.JOBID.err.TIMESTAMP
    delly_somatic.index_vcf.JOBID.out.TIMESTAMP
    delly_somatic.move_logs.JOBID.err.TIMESTAMP
    delly_somatic.move_logs.JOBID.out.TIMESTAMP
```  
  
There will be another set of .log files: 
 
```
    delly_somatic.log.TIMESTAMP
    delly_somatic.move_logs.log.TIMESTAMP
    delly_somatic.index.log.TIMESTAMP
    delly_somatic.bcf_to_vcf.log.TIMESTAMP
```
  
The *log file is useful to see in one go what steps where done. For instance the delly.log file only contains:

```
    #DONE_DELLY_SOMATIC
    commit_bb6db4a726171d6c84f177b0fdac5d54c8d7f752
``` 

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/somatic_SV-TIMESTAMP-session-name
```
- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'align' step failed because of memory (delly run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.

- **I forgot the most important bit. Where/What's the output?**


Let's sat your working ditectory is again:


```
/mnt/netapp2/mobilegenomes/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME
```

Then you will find the Unified Genotyper output:
                                                        
```
    delly.somatic.vcf.gz
    delly.somatic.vcf.gz.tbi
```
