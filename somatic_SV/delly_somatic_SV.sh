#!/bin/bash
#-----------------------------------------------------------------
# delly_somatic_SV.sh
# 
#
# Parameters : 
# $1: full path to referene genome fasta file
# $2: full path to the excluded regions file
# $3: full path to tumour bam file
# $4: full path to normal bam file
# $5: priority mode (normal,priority,xl,minimum) or custom config json file
# $6: --dryrun
#-----------------------------------------------------------------

# revert the command rm seccurity measure
unalias rm

# exit when any command fails
set -e

THIS_DIR=$(dirname $0)
THIS_PIPELINE=$(basename $THIS_DIR)

# print last commit information
cd $THIS_DIR
VERSION=`git log | head -n1 | sed 's/ /_/g'`

export PATH=/mnt/netapp2/uscmg_aplic/0_external_tools/tabix-0.2.6:$PATH

 
CLUSTER_CONFIG_JSON=`/bin/bash ${THIS_DIR}/../utils/apply_cluster_config.sh ${5} ${THIS_PIPELINE}` 
echo $CLUSTER_CONFIG_JSON
export CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_JSON


/mnt/netapp2/uscmg_aplic/0_external_tools/miniconda/3.6/bin/snakemake --snakefile  ${THIS_DIR}/delly_somatic_SV.sm  \
--config genome=${1} excluded_regions=${2}  tumour_bam=${3} normal_bam=${4} version=${VERSION} \
--cluster-config $CLUSTER_CONFIG_JSON \
--cluster "sbatch -p {cluster.p}  -A {cluster.account} --error {cluster.error} --output {cluster.output} --time  {cluster.time} --mem {cluster.mem} -c {cluster.c}" \
--printshellcmds --verbose --rerun-incomplete --nolock \
--latency-wait 60 \
--jobs 2 \
${6}
