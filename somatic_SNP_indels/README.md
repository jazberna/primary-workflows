 ## [somatic SNPs and indels](https://gitlab.com/mobilegenomesgroup/primary_pipelines/edit/labclinico_bioinfo/somatic_SNP_indels):

- **What is this?** This workflow runs [Mutect2](https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_cancer_m2_MuTect2.php) as described in their 'Tumor/Normal variant calling' section.
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/edit/labclinico_bioinfo/somatic_SNP_indels):
   
   * the actual snakemake workflow file [somatic_SNP_indels.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/somatic_SNP_indels/somatic_SNP_indels.sm)
   *In principle you should not need to edit this file as CPUs, RAM, time have been already specked out. Only in cases where for a particular reason the logs indicate that a particular rule (i.e. mutect2) failed i.e due to time*
   * the shell script that the user will eventually use to run this workflow: [mutect2.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/somatic_SNP_indels/mutect2.sh)
 
- **How do I run this thing?** The following example creates a screen session named 'jorgez_mutect2' runs the pipeline (mutect2.sh) and dettaches from the screen. mutect2.sh takes in this order:
	
	- fasta reference genome
	- tumour bam file
	- normal bam file
	- number of chromosomes to be run in parallel (I recommend 12)
	- priorty mode (normal or priority)

```
    screen -c /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/somatic_SNP_indels/screenrc \
    -L -S jorgez_mutect2 -dm /bin/bash /mnt/netapp2/uscmg_aplic/1_internal_tools/07_primary_pipelines/somatic_SNP_indels/mutect2.sh \
    /mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
    /mnt/lustre/scratch/home/usc/mg/jzb/TESTMUTECT2SPARK/4000_TRAFIC/2_alignment/HCC1143_T/aligned.sorted.marked_duplicates.bam \
    /mnt/lustre/scratch/home/usc/mg/jzb/TESTMUTECT2SPARK/4000_TRAFIC/2_alignment/HCC1143_N/aligned.sorted.marked_duplicates.bam \
    12 \
    priority

```
   
- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:

```
    /mnt/netapp2/mobilegenomes/0/1_projects/PROJECT_NAME/6_snv/TUMOUR_SAMPLE/logs/mutect2
```
  
  for example:
  
```
    /mnt/netapp2/mobilegenomes/0/1_projects/1050_LI-FRAUMENI-PE/6_snv/LFS3-tumor-MCRL007/logs/mutect2
  
```
  
  The logs will appear in such working directory. There will be two SLURM logs (.out.timestamp and .err.timestamp) per task namely.
  There will be also four *log.timestamp files useful to see in one go what steps where done. For instance the index.log.timestamp file only contains the string '#DONE_TABIX' and the version (commit) of the pipeline.
  
```
    chr.mutect2.log.timestamp
    mutect2.merge.log.timestamp
    mutect2.index.log.timestamp
    mutect2.move_logs.log.timestamp
```

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/somatic_SNP_indels-TIMESTAMP-session-name
```
  
- **Something went wrong. What do I do?** WWell, read the logs, find which job failed, let's say the 'align' step failed because of memory (bwa mem run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.
 
- **I forgot the most important bit. Where/What's the output?**

  The output will be in:
	
```
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/6_snv/TUMOUR_NAME
```
    
  For example:
    
```
    /mnt/netapp2/mobilegenomes/0/1_projects/1050_LI-FRAUMENI-PE/6_snv/LFS3-tumor-MCRL007
```

  Then you will find there, the Unified Genotyper output:

```
    mutect2.all_chrs.vcf.gz
    mutect2.all_chrs.vcf.idx
    mutect2.all_chrs.vcf.stats
    mutect2.all_chrs.vcf.gz.tbi
```

