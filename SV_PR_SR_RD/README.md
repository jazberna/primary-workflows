## [Structural Variation using Delly](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SV_PR_SR_RD/SV_PR_SR_RD):
- **What is this?** This workflow runs [Delly SV caller](https://github.com/dellytools/delly).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SV_PR_SR_RD):
   * the actual snakemake workflow file [CNV_PR_SR.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SV_PR_SR_RD/SV_PR_SR_RD.sm)
   * the shell script that the user will eventually use to run this workflow [CNV_PR_SR.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/SV_PR_SR_RD/delly_germinal.sh)
  
- **How do I run this thing?** The following example creates a screen session named 'jorgez_delly_germinal' runs the pipeline (delly_germinal.sh) and dettaches from the screen. delly_germinal.sh takes in this order:
	- reference genome file
	- excluded regions file
	- bam file 
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.

	
   I would do run the pipeline delly_germinal.sh like this:
	
```
	screen -c PATH_TO/SV_PR_SR_RD/screenrc \
	-L -S jorgez_delly_germinal -dm /bin/bash PATH_TO/SV_PR_SR_RD/delly_germinal.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa \
	/mnt/netapp2/uscmg_aplic/2_library/delly/hg19.excl \
	/mnt/netapp2/mobilegenomes/0/1_projects/1110_POH-PE/2_alignment/HS118/aligned.sorted.marked_duplicates.bam \
	priority
	
```
 
- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:

```
    /mnt/netapp2/mobilegenomes/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME/logs/delly_germinal
```

This workflow adds these SLURM files:

```  
    delly_germinal.JOBID.out.TIMESTAMP
    delly_germinal.JOBID.err.TIMESTAMP
    delly_germinal.bcf_to_vcf.JOBID.out.TIMESTAMP
    delly_germinal.bcf_to_vcf.JOBID.err.TIMESTAMP
    delly_germinal.index_vcf.JOBID.err.TIMESTAMP
    delly_germinal.index_vcf.JOBID.out.TIMESTAMP
    delly_germinal.move_logs.JOBID.err.TIMESTAMP
    delly_germinal.move_logs.JOBID.out.TIMESTAMP
```  
  
There will be another set of .log files: 
 
```
    delly_germinal.log.TIMESTAMP
    delly_germinal.move_logs.log.TIMESTAMP
    delly_germinal.index.log.TIMESTAMP
    delly_germinal.bcf_to_vcf.log.TIMESTAMP
```
  
The *log file is useful to see in one go what steps where done. For instance the delly.log file only contains:

```
    #DONE_DELLY_GERMINAL
    commit_bb6db4a726171d6c84f177b0fdac5d54c8d7f752
``` 

The screen log will be located in:
```
/mnt/netapp2/mobilegenomes/1/2_pipeline_logs/SV_PR_SR_RD-TIMESTAMP-session-name
```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'align' step failed because of memory (bwa mem run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.

- **I forgot the most important bit. Where/What's the output?**


Let's sat your working ditectory is again:


```
/mnt/netapp2/mobilegenomes/0/1_projects/PROJECT_NAME/5_sv/SAMPLE_NAME
```

Then you will find the Unified Genotyper output:
                                                        
```
    delly.germinal.vcf.gz
    delly.germinal.vcf.gz.tbi
```
