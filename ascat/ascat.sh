#!/bin/bash
#-----------------------------------------------------------------
# ascat.sh
# 
#
# Parameters : 
# $1: full path to referene genome fasta file
# $2: full path SnpGcCorrections.tsv
# $3: full path to tumour bam file 
# $4: full path to normal bam file 
# $5: gender (XX or XY)
# $6: protocol (WGS or WXS)
# $7: priority mode (normal,priority,xl,minimum) or custom config json file
# $8: --dryrun
#-----------------------------------------------------------------


# revert the command rm seccurity measure
unalias rm

# exit when any command fails
set -e

THIS_DIR=$(dirname $0)
THIS_PIPELINE=$(basename $THIS_DIR)

# print last commit information
cd $THIS_DIR
VERSION=`git log | head -n1 | sed 's/ /_/g'`


CLUSTER_CONFIG_JSON=`/bin/bash ${THIS_DIR}/../utils/apply_cluster_config.sh ${7} ${THIS_PIPELINE}` 
echo $CLUSTER_CONFIG_JSON
export CLUSTER_CONFIG_JSON=$CLUSTER_CONFIG_JSON

module load cesga
module load perl
module load gcc/6.4.0
module load pcap-core/3.5.0
module load allelecount/3.3.1
module load vcftools
module load samtools/1.9 

export PYTHONPATH=/mnt/netapp2/uscmg_aplic/0_external_tools/miniconda/3.6/lib/python3.6/site-packages:$PYTHONPATH

export PATH=$APLICACIONES/0_external_tools/miniconda/2.7/bin:$PATH
export PATH=$APLICACIONES/0_external_tools/ascatNgs/perl/bin:$PATH

export PERL5LIB=$APLICACIONES/0_external_tools/ascatNgs/perl/lib:$PERL5LIB
export PERL5LIB=$APLICACIONES/5_perl_libraries/lib/perl5:$PERL5LIB

export PATH=$APLICACIONES/0_external_tools/cgpVcf-2.0.4/bin:$PATH
export PERL5LIB=$APLICACIONES/0_external_tools/cgpVcf-2.0.4/lib/perl5:$PERL5LIB

export PATH=$APLICACIONES/0_external_tools/ascatNgs/bin:$PATH
export PERL5LIB=$APLICACIONES/0_external_tools/ascatNgs/lib/perl5:$PERL5LIB

export CGP_PERLLIBS=$PERL5LIB


/mnt/netapp2/uscmg_aplic/0_external_tools/miniconda/3.6/bin/snakemake --snakefile ${THIS_DIR}/ascat.sm  \
--config reference=${1} snpgccorrection=${2} tumour_bam=${3} normal_bam=${4} gender=${5} protocol=${6} version=${VERSION} \
--cluster-config $CLUSTER_CONFIG_JSON \
--cluster "sbatch -p {cluster.p}  -A {cluster.account} --error {cluster.error} --output {cluster.output} --time  {cluster.time} --mem {cluster.mem} -c {cluster.c}" \
--printshellcmds  --rerun-incomplete --verbose --latency-wait 60 --jobs 1 \
${8}

