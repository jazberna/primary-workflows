## [Somatic Copy Number Variation using ASCAT NGS](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/ascat):
- **What is this?** This workflow runs [ASCAT NGS](https://github.com/cancerit/ascatNgs).
- **Where is it?** The workflow, shell scripts and config files for this pipeline are contained [here](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/ascat/):
   * the actual snakemake workflow file [ascat.sm](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/ascat/ascat.sm)
   * the shell script that the user will eventually use to run this workflow [ascat.sh](https://gitlab.com/mobilegenomesgroup/primary_pipelines/blob/labclinico_bioinfo/ascat/ascat.sh)

- **How do I run this thing?** The following example creates a screen session named 'jorgez_ascat' runs the script (ascat.sh) and dettaches from the screen. ascat.sh takes in this order:
	- full path to referene genome fasta.fai file
	- full path to the SnpGcCorrections.tsv file:
		* GRCh37: $APLICACIONES/2_library/ascatngs/digital_karyotypes/SnpGcCorrections.tsv
		* GRCh38: $APLICACIONES/2_library/ascatngs/digital_karyotypes/SnpGcCorrections.GRCh38_keiran.tsv
	- full path to tumour bam file
	- full path to normal bam file 
	- experiment type. Actually this is WGS, or WXS (for exomes)
	- gender, either XX or XY
	- cluster mode. The cluster mode can be 'normal', 'priority', 'xl' and 'minimum'. I recommed to use 'priority'.
	- --dryrun option. add that that if you only want to see the execution plan.
   I would do run the pipeline ascat.sh like this:
	
```
	screen -c /mnt/netapp2/uscmg_aplic/1_internal_tools/primary_pipelines/ascat/screenrc \
	-L -S jorgez_ascat -dm /bin/bash /mnt/netapp2/uscmg_aplic/1_internal_tools/primary_pipelines/ascat/ascat.sh \
	/mnt/netapp2/mobilegenomes/0/0_reference/0_homo_sapiens/hg_19_hs37d5/hs37d5.fa.fai \
	/mnt/netapp2/uscmg_aplic/2_library/ascatngs/digital_karyotypes/SnpGcCorrections.tsv \
	/mnt/lustre/scratch/home/usc/mg/jzb/TESTASCAT/PROJECT/2_alignment/TUMOUR/aligned.sorted.marked_duplicates.bam \
	/mnt/lustre/scratch/home/usc/mg/jzb/TESTASCAT/PROJECT/2_alignment/NORMAL/aligned.sorted.marked_duplicates.bam \
	XX \
	WGS \
	priority
```

- **Where are the logs?** The logs you are interested in are produced in the working directory for this this workflow which has this pattern:
```
    $MOBILEGENOMES/0/1_projects/PROJECT_NAME/7_cnv/TUMOUR_SAMPLE/logs/ascat
```
The SLURM logs would be:

```
	ascat.JOBID.err.TIMESTAMP
	ascat.JOBID.out.TIMESTAMP
	ascat.JOBID.log.TIMESTAMP
```

  There will two extra logs that contain the version (commit) used to run the workflow:

```
ascat.log.TIMESTAMP
ascat.move_logs.log.TIMESTAMP
```

  The ascat.log.TIMESTAMP files is useful to see in one go what steps were done, it reads something like:
	
```
	#DONE_ALLELECOUNT
	commit_7b876321e698e7af3a96f1ba432e39c56bb6d02a
	#DONE_ASCAT
	commit_7b876321e698e7af3a96f1ba432e39c56bb6d02a
```

The screen log will be located in:
```
    /mnt/netapp2/mobilegenomes/1/2_pipeline_logs/ascat-TIMESTAMP-session-name
```

- **Something went wrong. What do I do?** Well, read the logs, find which job failed, let's say the 'ascat' step failed because of memory (ascat run out of memory). Then rerun the pipeline with the 'xl' mode (instead of 'priority'). Snakemake will pick it up from where it failed.

- **I forgot the most important bit. Where/What's the output?**

  Let's sat your working ditectory is again:

```
	$MOBILEGENOMES/0/1_projects/PROJECT_NAME/7_cnv/TUMOUR_NAME/ascat/tmpAscat/ascat
```

  Then you will find the output:

```
	H2009.ASCATprofile.png
	H2009.ASPCF.png
	H2009.germline.png
	H2009.rawprofile.png
	H2009.sunrise.png
	H2009.tumour.png
```
